def onefork(tests, helpers):
    grade = 0
    cmts = set()
    mytests = [t for t in tests if "onefork" in t.name]
    for test in mytests:
        try:
            res = test.results().split('\n')
            i = res.index("OS/161 kernel: p uw-testbin/onefork")
            if "Operation took" in res[i+1]:
                cmts.add("The output of onefork is empty.")
            elif "Operation took" in res[i+2]:
                if res[i+1] == "PC" or res[i+1] == "CP":
                    grade += 1
                else:
                    cmts.add("The output of onefork is wrong:")
                    cmts.add(res[i+1])
            elif "Operation took" in res[i+3]:
                if (res[i+1] == "P" and res[i+2] == "C") or (res[i+1] == "C" and res[i+2] == "P"):
                    grade += 1
                else:
                    cmts.add("The output of onefork is wrong:")
                    cmts.add(res[i+1])
                    cmts.add(res[i+2])
            else:
                cmt.add("The output of onefork is too long.")
        except:
            cmts.add("Cannot parse assignment log - likely an error in your assignment")
        
    return ("Test onefork", grade, ",".join(cmts))

def pidcheck(tests, helpers):
    grade = 0
    cmts = set()
    mytests = [t for t in tests if "pidcheck" in t.name]
    for test in mytests:
        try:
            res = test.results().split('\n')
            i = res.index("OS/161 kernel: p uw-testbin/pidcheck")
            pids = {}
            pid_names = ["C", "PC", "PP"]
            error = False
            if "Operation took" in res[i+4]:
                for j in [1, 2, 3]:
                    entry = res[i+j].split(":")
                    k = entry[0].strip()
                    v = int(entry[1].strip())
                    # print("({}, {})".format(k, v))
                    if k in pids:
                        error = True
                        cmts.add("Duplicate pid output of {}".format(k))
                    if not k in pid_names:
                        error = True
                        cmts.add("Unknown pid name: {}".format(k))
                    elif v <= 0:
                        error = True
                        cmts.add("The value of {} must be positive: {}".format(k, v))
                    else:
                        pids[k] = v

                if not error and len(pids) != 3:
                    error = True
                    cmts.add("The number of pids are wrong: {}".format(list(pids.keys())))
                
                if not error: 
                    if pids["C"] == pids["PC"] and pids["C"] != pids["PP"]:
                        grade += 1
                    else:
                        grade += 0.5
                        cmts.add("pids do not match")
            else:
                cmts.add("The length of output is wrong.")
        except Exception as e:
            cmts.add("Cannot parse assignment log - likely an error in your assignment")
        
    return ("Test pidcheck", grade, ",".join(cmts))

def widefork(tests, helpers):
    grade = 0
    cmts = set()
    mytests = [t for t in tests if "widefork" in t.name]
    for test in mytests:
        try:
            res = test.results().split('\n')
            i = res.index("OS/161 kernel: p uw-testbin/widefork")
            j = 1
            s = []
            while not "Operation took" in res[i+j]:
                for c in res[i+j].strip():
                    s.append(c)
                j = j + 1

            P_num = 0 
            A_num = 0
            B_num = 0
            C_num = 0
            a_num = 0
            b_num = 0
            c_num = 0
            x_num = 0
            error = False
            for c in s:
                if c == 'P':
                    P_num = P_num + 1
                    if P_num > 3:
                        error = True
                        cmts.add("Too many P")
                        break
                elif c == 'A':
                    if P_num == 0:
                        error = True
                        cmts.add("A must be preceded by one P")
                        break
                    elif A_num == 1:
                        error = True
                        cmts.add("There should be one A only.")
                        break
                    else:
                        P_num -= 1
                        A_num = 1
                elif c == 'B':
                    if P_num == 0:
                        error = True
                        cmts.add("B found but not enough P's (should be at LEAST 2)")
                        break
                    elif B_num == 1:
                        error = True
                        cmts.add("There should be one B only.")
                        break
                    else:
                        B_num = 1
                        P_num -= 1
                elif c == 'C':
                    if P_num == 0:
                        error = True
                        cmts.add("C found but not enough P's (should be at LEAST 3)")
                        break
                    elif C_num == 1:
                        error = True
                        cmts.add("There should be one C only.")
                        break
                    else:
                        C_num = 1
                        P_num -= 1
                elif c == 'a':
                    if A_num != 1:
                        error = True
                        cmts.add("a must be preceded by A")
                        break
                    elif a_num != 0:
                        error = True
                        cmts.add("There should be one 'a' only.")
                        break
                    else:
                        a_num = 1
                elif c == 'b':
                    if B_num != 1:
                        error = True
                        cmts.add("b must be preceded by B")
                        break
                    elif b_num != 0:
                        error = True
                        cmts.add("There should be one 'b' only.")
                        break
                    else:
                        b_num = 1
                elif c == 'c':
                    if C_num != 1:
                        error = True
                        cmts.add("c must be preceded by C")
                        break
                    elif c_num != 0:
                        error = True
                        cmts.add("There should be one 'c' only.")
                        break
                    else:
                        c_num = 1
                elif c == 'x':
                    x_num = x_num + 1
                else:
                    error = True
                    cmts.add("Wrong output: {}".format(c))
                    break
            
            if error:
                cmts.add("Incorrect output: {}".format("".join(s)))
            elif P_num == 0 and A_num == 1 and B_num == 1 and C_num == 1 and len(s) == 9 and x_num > 0:
                grade += 1
                cmts.add("Partial correct output: {}".format("".join(s)))
            elif a_num == 1 and b_num == 1 and c_num == 1 and len(s) == 9:
                grade += 2
            else:
                cmts.add("Incorrect output: {}".format("".join(s)))
        except Exception as e:
            print(e)
            cmts.add("Cannot parse assignment log - likely an error in your assignment")
        
    return ("Test widefork", grade, ",".join(cmts))

def forktest(tests, helpers):
    grade = 0
    cmts = set()
    mytests = [t for t in tests if "forktest" in t.name]
    for test in mytests:      
        try:
            res = test.results().split('\n')
            i = res.index("OS/161 kernel: p testbin/forktest")
            if "Starting." in res[i+1] and \
                "Complete." in res[i+3] and \
                "Operation took" in res[i+4] :
                s = res[i+2].strip()
                if len(s) != 30:
                    cmts.add("Wrong output length: {}".format(s))
                elif s.count('0') == 2 and \
                    s.count('1') == 4 and \
                    s.count('2') == 8 and \
                    s.count('3') == 16:
                    grade += 2
                else:
                    cmts.add("Wrong output: {}".format(s))
            else:
                cmts.add("Wrong output format.")
        except Exception as e:
            print(e)
            cmts.add("Cannot parse assignment log - likely an error in your assignment")
        
    return ("Test forktest", grade, ",".join(cmts))

def argtest_none(tests, helpers):
    grade = 0
    cmts = set()
    mytests = [t for t in tests if "argtest-none" in t.name]
    for test in mytests:
        try:
            res = test.results().split('\n')
            i = res.index("OS/161 kernel: p uw-testbin/argtest")

            if "Operation took" in res[i+1]:
                cmts.add("The output of argtest-none is empty.")

            elif "Operation took" in res[i+13]:
                operation_took_index = i + 13

                if ("argc:1" in res[i + 1].replace(" ", "") and
                    "argv[1] -> [NULL]" in res[operation_took_index - 1] and 
                    "argv[0] -> uw-testbin/argtest" in res[operation_took_index - 2]):
                        grade+=2
                else:
                    print('\n'.join(res))
                    cmts.add("The output of argtest-none is wrong:")
            else:
                cmts.add("The output of argtest-none is too long or incorrect.")
        except Exception as e:
            cmts.add("Cannot parse assignment log - likely an error in your assignment")
        
    return ("Test argtest-none", grade, ",".join(cmts))

def sty(tests, helpers):
    grade = 0
    cmts = set()
    mytests = [t for t in tests if "sty" in t.name]
    for test in mytests:
        try:
            res = test.results().split('\n')
            i = res.index("OS/161 kernel: p testbin/sty")

            if ("Operation took" in res[i+1] and
                    "OS/161 kernel: q" in res[i + 2] and
                    "Shutting down" in res[i + 3]):
                grade+=9
            else:
                cmts.add("The output of sty is incorrect")
                #cmts.add(str(res))
        except Exception as e:
            cmts.add("Cannot parse assignment log - likely an error in your assignment")
        
    return ("Test sty", grade, ",".join(cmts))

def add(tests, helpers):
    grade = 0
    cmts = set()
    mytests = [t for t in tests if "add" in t.name]
    for test in mytests:
        try:
            res = test.results().split('\n')
            i = res.index("OS/161 kernel: p testbin/add 897 78")

            if "Operation took" in res[i+1]:
                cmts.add("The output of add is empty.")

            elif "Operation took" in res[i+2]:
                if "Answer: 975" in str(res[i+1]):
                    grade += 4
                else:
                    cmts.add("The output of add is wrong:")
                    cmts.add(res[i+1])
            else:
                cmts.add("The output of add is too long or incorrect.")
        except Exception as e:
            cmts.add("Cannot parse assignment log - likely an error in your assignment")
        
    return ("Test add", grade, ",".join(cmts))

def argtesttest(tests, helpers):
    grade = 0
    cmts = set()
    mytests = [t for t in tests if "argtesttest" in t.name]
    for test in mytests:
        try:
            res = test.results().split('\n')
            i = res.index("OS/161 kernel: p uw-testbin/argtesttest")

            if "Operation took" in res[i+1]:
                cmts.add("The output of argtesttest is empty.")

            elif "Operation took" in res[i+6]:
                operation_took_index = i + 6

                if ("argc:3" in res[i + 1].replace(" ", "") and
                    "argv[3]: [NULL]" in res[operation_took_index - 1] and 
                    "argv[2]: second" in res[operation_took_index - 2] and
                    "argv[1]: first" in res[operation_took_index - 3] and
                    "argv[0]: argtesttest" in res[operation_took_index - 4]):
                        grade+=4
                else:
                    print('\n'.join(res))
                    cmts.add("The output of argtesttest is wrong:")
            else:
                cmts.add("The output of argtesttest is too long or incorrect.")
        except Exception as e:
            cmts.add("Cannot parse assignment log - likely an error in your assignment")
        
    return ("Test argtesttest", grade, ",".join(cmts))

def hogparty(tests, helpers):
    grade = 0
    cmts = set()
    mytests = [t for t in tests if "hogparty_custom_0" in t.name]
    for test in mytests:
        try:
            res = test.results().split('\n')
            i = res.index("OS/161 kernel: p uw-testbin/hogparty")

            if "Operation took" in res[i+1]:
                cmts.add("The output of hogparty is empty.")

            elif "Operation took" in res[i+4]:
                op_strs = res[i + 1] + res[i + 2] + res[i + 3]

                if op_strs.count('x') == 5 and op_strs.count('y') == 5 and op_strs.count('z') == 5:
                    grade+=9
                else:
                    print('\n'.join(res))
                    cmts.add("The output of hogparty is wrong:")
            elif "Operation took" in res[i+3]:
                op_strs = res[i + 1] + res[i + 2]

                if op_strs.count('x') == 5 and op_strs.count('y') == 5 and op_strs.count('z') == 5:
                    grade+=9
                else:
                    print('\n'.join(res))
                    cmts.add("The output of hogparty is wrong:")

            elif "Operation took" in res[i+2]:
                op_strs = res[i + 1]

                if op_strs.count('x') == 5 and op_strs.count('y') == 5 and op_strs.count('z') == 5:
                    grade+=9
                else:
                    print('\n'.join(res))
                    cmts.add("The output of hogparty is wrong:")
            else:
                cmts.add("The output of hogparty is too long or incorrect.")
        except Exception as e:
            cmts.add("Cannot parse assignment log - likely an error in your assignment")
        
    return ("Test hogparty", grade, ",".join(cmts))

def argtest_long(tests, helpers):
    grade = 0
    cmts = set()
    mytests = [t for t in tests if "argtest-long" in t.name]
    for test in mytests:
        try:
            res = test.results().split('\n')
            i = res.index("OS/161 kernel: p uw-testbin/argtest thisisalongishargthatisjustreallylongbecauseitislog short")

            if "Operation took" in res[i+1]:
                cmts.add("The output of argtest-long is empty.")

            elif "Operation took" in res[i+19]:
                operation_took_index = i + 19

                if ("argc:3" in res[i + 1].replace(" ", "") and
                    "argv[3] -> [NULL]" in res[operation_took_index - 1] and 
                    "argv[2] -> short" in res[operation_took_index - 2] and
                    "argv[1] -> thisisalongishargthatisjustreallylongbecauseitislog" in res[operation_took_index - 3] and
                    "argv[0] -> uw-testbin/argtest" in res[operation_took_index - 4]):
                        grade+=2
                else:
                    print('\n'.join(res))
                    cmts.add("The output of argtest-long is wrong:")
            else:
                cmts.add("The output of argtest-long is too long or incorrect.")
        except Exception as e:
            cmts.add("Cannot parse assignment log - likely an error in your assignment")
        
    return ("Test argtest-long", grade, ",".join(cmts))

def argtest_many(tests, helpers):
    grade = 0
    cmts = set()
    mytests = [t for t in tests if "argtest-many" in t.name]
    for test in mytests:
        try:
            res = test.results().split('\n')
            i = res.index("OS/161 kernel: p uw-testbin/argtest 456 howaboutthis string and another yet again")

            if "Operation took" in res[i+1]:
                cmts.add("The output of argtest-many is empty.")

            elif "Operation took" in res[i+34]:
                operation_took_index = i + 34

                if ("argc:8" in res[i + 1].replace(" ", "") and
                    "argv[8] -> [NULL]" in res[operation_took_index - 1] and 
                    "argv[7] -> again" in res[operation_took_index - 2] and
                    "argv[6] -> yet" in res[operation_took_index - 3] and
                    "argv[5] -> another" in res[operation_took_index - 4] and
                    "argv[4] -> and" in res[operation_took_index - 5] and
                    "argv[3] -> string" in res[operation_took_index - 6] and
                    "argv[2] -> howaboutthis" in res[operation_took_index - 7] and
                    "argv[1] -> 456" in res[operation_took_index - 8]):
                        grade+=2
                else:
                    print(res)
                    cmts.add("The output of argtest-many is wrong:")
            else:
                cmts.add("The output of argtest-many is too long or incorrect.")
        except Exception as e:
            cmts.add("Cannot parse assignment log - likely an error in your assignment")
        
    return ("Test argtest-many", grade, ",".join(cmts))

RUBRIC = {
    onefork: 3,
    pidcheck: 3,
    widefork: 6,
    forktest: 6,
    argtesttest: 4,
    hogparty: 9,
    sty: 9,
    add: 4,
    argtest_none: 2,
    argtest_long: 2,
    argtest_many: 2
}
