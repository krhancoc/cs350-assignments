def sy2(tests, helpers):
    grade = 0
    cmts = set()
    mytests = [t for t in tests if "sy2" in t.name]
    if len(mytests) != 3:
        cmts.add("Not all configurations were run - likly timed out")

    for test in mytests:
        try:
            res = test.results().split('\n')
            i = res.index("OS/161 kernel: sy2")

            if "Operation took" in res[i+1]:
                cmts.add("The output of sy2 is empty.")

            elif "Operation took" in res[i+4]:
                if "lock test done" in str(res[i+3]).lower():
                    grade += 8
                else:
                    cmts.add("The output of sy2 is wrong:")
                    cmts.add(res[i+1])
            else:
                cmts.add("The output of sy2 is too long or incorrect.")
        except Exception as e:
            cmts.add("Cannot parse assignment log - likely an error in your assignment")
        
    return ("Test sy2", grade, ",".join(cmts))

def sy3(tests, helpers):
    grade = 0
    cmts = set()
    mytests = [t for t in tests if "sy3" in t.name]
    if len(mytests) != 3:
        cmts.add("Not all configurations were run - likly timed out")

    for test in mytests:
        try:
            res = test.results().split('\n')
            i = res.index("OS/161 kernel: sy3")
            end = res.index("CV test done")

            if "Operation took" in res[i+1]:
                cmts.add("The output of sy3 is empty.")

            elif "CV test done" in res:
                counter = 31
                iterations = 5
                for entry in res[i+3:end-1]:
                    if int(entry.lower().split()[1]) == counter:
                        counter-=1

                    if counter == 0:
                        counter = 31
                        iterations-=1
                
                if iterations == 0:
                    grade+=8
            else:
                cmts.add("The output of sy3 is incorrect.")

        except Exception as e:
            cmts.add("Cannot parse assignment log - likely an error in your assignment")
        
    return ("Test sy3", grade, ",".join(cmts))

def uw1(tests, helpers):
    grade = 0
    cmts = set()
    mytests = [t for t in tests if "uw1" in t.name]
    if len(mytests) != 3:
        cmts.add("Not all configurations were run - likly timed out")

    for test in mytests:
        try:
            res = test.results().split('\n')
            i = res.index("OS/161 kernel: uw1")

            if "Operation took" in res[i+1]:
                cmts.add("The output of uw1 is empty.")
            elif "Operation took" in res[i+6]:
                if "test succeeded" in str(res[i+3]).lower():
                    grade += 8
                else:
                    cmts.add("The output of uw1 is wrong:")
            else:
                cmts.add("The output of uw1 is too long or incorrect.")

        except Exception as e:
            cmts.add("Cannot parse assignment log - likely an error in your assignment")
        
    return ("Test uw1", grade, ",".join(cmts))

def single_vehicle_no_bias(tests, helpers):
    grade = 0
    cmts = set()
    mytests = [t for t in tests if "single-vehicle-no-bias" in t.name]
    if len(mytests) != 3:
        cmts.add("Not all configurations were run - likly timed out")

    for test in mytests:
        try:
            res = test.results().split('\n')
            i = res.index("OS/161 kernel: sp3 1 150 1 1 0")

            if "Operation took" in res[i+1]:
                cmts.add("The output of single-vehicle-no-bias is empty.")
            elif "Operation took" in res[i+9]:
                north   = res[i+3].split(",")
                east    = res[i+4].split(",")
                south   = res[i+5].split(",")
                west    = res[i+6].split(",")

                north_avg = float(north[1].split()[2])
                north_max = float(north[2].split()[2])

                east_avg = float(east[1].split()[2])
                east_max = float(east[2].split()[2])

                south_avg = float(south[1].split()[2])
                south_max = float(south[2].split()[2])

                west_avg = float(west[1].split()[2])
                west_max = float(west[2].split()[2])

                if north_avg == 0 and east_avg == 0 and south_avg == 0 and west_avg == 0:
                    grade += 2
                else:
                    cmts.add("The output of single-vehicle-no-bias sp3 1 150 1 1 0 is either wrong or does not meet efficiency thresholds:")
            else:
                cmts.add("The output of single-vehicle-no-bias is too long or incorrect.")

        except Exception as e:
            cmts.add("Cannot parse assignment log - likely an error in your assignment")
        
    return ("Test single-vehicle-no-bias", grade, ",".join(cmts))

def single_vehicle_with_bias(tests, helpers):
    grade = 0
    cmts = set()
    mytests = [t for t in tests if "single-vehicle-with-bias" in t.name]
    if len(mytests) != 3:
        cmts.add("Not all configurations were run - likly timed out")

    for test in mytests:
        try:
            res = test.results().split('\n')
            i = res.index("OS/161 kernel: sp3 1 150 1 1 1")

            if "Operation took" in res[i+1]:
                cmts.add("The output of single-vehicle-with-bias is empty.")
            elif "Operation took" in res[i+9]:
                north   = res[i+3].split(",")
                east    = res[i+4].split(",")
                south   = res[i+5].split(",")
                west    = res[i+6].split(",")

                north_avg = float(north[1].split()[2])
                north_max = float(north[2].split()[2])

                east_avg = float(east[1].split()[2])
                east_max = float(east[2].split()[2])

                south_avg = float(south[1].split()[2])
                south_max = float(south[2].split()[2])

                west_avg = float(west[1].split()[2])
                west_max = float(west[2].split()[2])

                if north_avg == 0 and east_avg == 0 and south_avg == 0 and west_avg == 0:
                    grade += 2
                else:
                    cmts.add("The output of single-vehicle-with-bias sp3 1 150 1 1 1 is either wrong or does not meet efficiency thresholds:")
            else:
                cmts.add("The output of single-vehicle-with-bias is too long or incorrect.")

        except Exception as e:
            cmts.add("Cannot parse assignment log - likely an error in your assignment")
        
    return ("Test single-vehicle-with-bias", grade, ",".join(cmts))

def light_load_no_bias(tests, helpers):
    grade = 0
    cmts = set()
    mytests = [t for t in tests if "light-load-no-bias" in t.name]
    if len(mytests) != 3:
        cmts.add("Not all configurations were run - likly timed out")

    for test in mytests:
        try:
            res = test.results().split('\n')
            i = res.index("OS/161 kernel: sp3 5 100 10 1 0")

            if "Operation took" in res[i+1]:
                cmts.add("The output of light-load-no-bias is empty.")
            elif "Operation took" in res[i+9]:
                north   = res[i+3].split(",")
                east    = res[i+4].split(",")
                south   = res[i+5].split(",")
                west    = res[i+6].split(",")

                north_avg = float(north[1].split()[2])
                north_max = float(north[2].split()[2])

                east_avg = float(east[1].split()[2])
                east_max = float(east[2].split()[2])

                south_avg = float(south[1].split()[2])
                south_max = float(south[2].split()[2])

                west_avg = float(west[1].split()[2])
                west_max = float(west[2].split()[2])

                if ((north_avg + east_avg + south_avg + west_avg)/4) < 0.010:
                    grade += 3
                else:
                    cmts.add("The output of light-load-no-bias sp3 5 100 10 1 0 is either wrong or does not meet efficiency thresholds:")
            else:
                cmts.add("The output of light-load-no-bias is too long or incorrect.")

        except Exception as e:
            cmts.add("Cannot parse assignment log - likely an error in your assignment")
        
    return ("Test light-load-no-bias", grade, ",".join(cmts))

def light_load_with_bias(tests, helpers):
    grade = 0
    cmts = set()
    mytests = [t for t in tests if "light-load-with-bias" in t.name]
    if len(mytests) != 3:
        cmts.add("Not all configurations were run - likly timed out")

    for test in mytests:
        try:
            res = test.results().split('\n')
            i = res.index("OS/161 kernel: sp3 5 100 10 1 1")

            if "Operation took" in res[i+1]:
                cmts.add("The output of light-load-with-bias is empty.")
            elif "Operation took" in res[i+9]:
                north   = res[i+3].split(",")
                east    = res[i+4].split(",")
                south   = res[i+5].split(",")
                west    = res[i+6].split(",")

                north_avg = float(north[1].split()[2])
                north_max = float(north[2].split()[2])

                east_avg = float(east[1].split()[2])
                east_max = float(east[2].split()[2])

                south_avg = float(south[1].split()[2])
                south_max = float(south[2].split()[2])

                west_avg = float(west[1].split()[2])
                west_max = float(west[2].split()[2])

                if (north_avg + east_avg + south_avg + west_avg)/4 < 0.010:
                    grade += 2
                else:
                    cmts.add("The output of light-load-with-bias sp3 5 100 10 1 1 is either wrong or does not meet efficiency thresholds:")
            else:
                cmts.add("The output of light-load-with-bias is too long or incorrect.")

        except Exception as e:
            cmts.add("Cannot parse assignment log - likely an error in your assignment")
        
    return ("Test light-load-with-bias", grade, ",".join(cmts))

def heavy_load1_no_bias(tests, helpers):
    grade = 0
    cmts = set()
    mytests = [t for t in tests if "heavy-load-1-no-bias" in t.name]
    if len(mytests) != 3:
        cmts.add("Not all configurations were run - likly timed out")

    for test in mytests:
        try:
            res = test.results().split('\n')
            i = res.index("OS/161 kernel: sp3 2 300 0 1 0")

            if "Operation took" in res[i+1]:
                cmts.add("The output of heavy-load-1-no-bias is empty.")
            elif "Operation took" in res[i+9]:
                north   = res[i+3].split(",")
                east    = res[i+4].split(",")
                south   = res[i+5].split(",")
                west    = res[i+6].split(",")

                north_avg = float(north[1].split()[2])
                north_max = float(north[2].split()[2])

                east_avg = float(east[1].split()[2])
                east_max = float(east[2].split()[2])

                south_avg = float(south[1].split()[2])
                south_max = float(south[2].split()[2])

                west_avg = float(west[1].split()[2])
                west_max = float(west[2].split()[2])

                simulation_time = float(res[i+8].split()[1])

                if simulation_time < 6:
                    grade+=1.5

                if (north_max < 0.018 and
                    east_max < 0.018 and
                    south_max < 0.018 and
                    west_max < 0.018):
                    grade += 1.5

                if grade < 1.5:
                    cmts.add("The output of heavy-load-1-no-bias is wrong")
            else:
                cmts.add("The output of heavy-load-1-no-bias is too long or incorrect.")

        except Exception as e:
            cmts.add("Cannot parse assignment log - likely an error in your assignment")
    
    if grade < 9:
       cmts.add("The output of heavy-load-1-no-bias sp3 2 300 0 1 0 does not meet efficiency thresholds set")

    return ("Test heavy-load-1-no-bias", grade, ",".join(cmts))

def heavy_load1_with_bias(tests, helpers):
    grade = 0
    cmts = set()
    mytests = [t for t in tests if "heavy-load-1-with-bias" in t.name]
    if len(mytests) != 3:
        cmts.add("Not all configurations were run - likly timed out")

    for test in mytests:
        try:
            res = test.results().split('\n')
            i = res.index("OS/161 kernel: sp3 2 300 0 1 1")

            if "Operation took" in res[i+1]:
                cmts.add("The output of heavy-load-1-with-bias is empty.")
            elif "Operation took" in res[i+9]:
                north   = res[i+3].split(",")
                east    = res[i+4].split(",")
                south   = res[i+5].split(",")
                west    = res[i+6].split(",")

                north_avg = float(north[1].split()[2])
                north_max = float(north[2].split()[2])

                east_avg = float(east[1].split()[2])
                east_max = float(east[2].split()[2])

                south_avg = float(south[1].split()[2])
                south_max = float(south[2].split()[2])

                west_avg = float(west[1].split()[2])
                west_max = float(west[2].split()[2])

                simulation_time = float(res[i+8].split()[1])

                if simulation_time < 6:
                    grade+=1

                if (north_max < 0.018 and
                    east_max < 0.018 and
                    south_max < 0.018 and
                    west_max < 0.018):
                    grade += 1

                if grade < 1:
                    cmts.add("The output of heavy-load-1-with-bias is wrong:")
            else:
                cmts.add("The output of heavy-load-1-with-bias is too long or incorrect.")

        except Exception as e:
            cmts.add("Cannot parse assignment log - likely an error in your assignment")

    if grade < 6:
       cmts.add("The output of heavy-load-1-with-bias sp3 2 300 0 1 1 does not meet efficiency thresholds set")
        
    return ("Test heavy-load-1-with-bias", grade, ",".join(cmts))

L2_EFFICIENCY = 0.100
L2_GAP = 0.015
def heavy_load2_no_bias(tests, helpers):
    grade = 0
    cmts = set()
    mytests = [t for t in tests if "heavy-load-2-no-bias" in t.name]
    if len(mytests) != 3:
        cmts.add("Not all configurations were run - likly timed out")

    for test in mytests:
        try:
            res = test.results().split('\n')
            i = res.index("OS/161 kernel: sp3 6 100 0 1 0")

            if "Operation took" in res[i+1]:
                cmts.add("The output of heavy-load-2-no-bias is empty.")
            elif "Operation took" in res[i+9]:
                north   = res[i+3].split(",")
                east    = res[i+4].split(",")
                south   = res[i+5].split(",")
                west    = res[i+6].split(",")

                north_avg = float(north[1].split()[2])
                north_max = float(north[2].split()[2])

                east_avg = float(east[1].split()[2])
                east_max = float(east[2].split()[2])

                south_avg = float(south[1].split()[2])
                south_max = float(south[2].split()[2])

                west_avg = float(west[1].split()[2])
                west_max = float(west[2].split()[2])

                simulation_time = float(res[i+8].split()[1])

                overall_avg = (north_avg + east_avg + south_avg + west_avg)/4

                if simulation_time < 5:
                    grade+=1.5

                if (    (north_max < L2_EFFICIENCY and east_max < L2_EFFICIENCY and south_max < L2_EFFICIENCY and west_max < L2_EFFICIENCY) and
                        (abs(north_avg - overall_avg) < L2_GAP and abs(east_avg - overall_avg) < L2_GAP and
                        abs(south_avg - overall_avg) < L2_GAP and abs(west_avg - overall_avg) < L2_GAP)
                    ):
                    grade += 1.5

                if grade < 1.5:
                    cmts.add("The output of heavy-load-2-no-bias is wrong:")
            else:
                cmts.add("The output of heavy-load-2-no-bias is too long or incorrect.")

        except Exception as e:
            cmts.add("Cannot parse assignment log - likely an error in your assignment")

    if grade < 9:
       cmts.add("The output of heavy-load-2-no-bias sp3 6 100 0 1 0 does not meet efficiency thresholds set")
        
    return ("Test heavy-load-2-no-bias", grade, ",".join(cmts))

def heavy_load2_with_bias(tests, helpers):
    grade = 0
    cmts = set()
    mytests = [t for t in tests if "heavy-load-2-with-bias" in t.name]
    if len(mytests) != 3:
        cmts.add("Not all configurations were run - likly timed out")

    for test in mytests:
        try:
            res = test.results().split('\n')
            i = res.index("OS/161 kernel: sp3 6 100 0 1 1")

            if "Operation took" in res[i+1]:
                cmts.add("The output of heavy-load-2-with-bias is empty.")
            elif "Operation took" in res[i+9]:
                north   = res[i+3].split(",")
                east    = res[i+4].split(",")
                south   = res[i+5].split(",")
                west    = res[i+6].split(",")

                north_avg = float(north[1].split()[2])
                north_max = float(north[2].split()[2])

                east_avg = float(east[1].split()[2])
                east_max = float(east[2].split()[2])

                south_avg = float(south[1].split()[2])
                south_max = float(south[2].split()[2])

                west_avg = float(west[1].split()[2])
                west_max = float(west[2].split()[2])

                simulation_time = float(res[i+8].split()[1])

                if simulation_time < 5:
                    grade+=1

                if (north_max < L2_EFFICIENCY and
                    east_max < L2_EFFICIENCY and
                    south_max < L2_EFFICIENCY and
                    west_max < L2_EFFICIENCY):
                    grade += 1

                if grade < 1:
                    cmts.add("The output of heavy-load-2-with-bias is wrong:")
            else:
                cmts.add("The output of heavy-load-2-with-bias is too long or incorrect.")

        except Exception as e:
            cmts.add("Cannot parse assignment log - likely an error in your assignment, or ran out of time")

    if grade < 6:
       cmts.add("The output of heavy-load-2-with-bias sp3 6 100 0 1 1 does not meet efficiency thresholds set")
        
    return ("Test heavy-load-2-with-bias", grade, ",".join(cmts))


L3_EFFICIENCY = 0.150
L3_GAP = 0.015
def heavy_load3_no_bias(tests, helpers):
    grade = 0
    cmts = set()
    mytests = [t for t in tests if "heavy-load-3-no-bias" in t.name]
    if len(mytests) != 3:
        cmts.add("Not all configurations were run - likly timed out")

    for test in mytests:
        try:
            res = test.results().split('\n')
            i = res.index("OS/161 kernel: sp3 10 80 0 1 0")

            if "Operation took" in res[i+1]:
                cmts.add("The output of heavy-load-3-no-bias is empty.")
            elif "Operation took" in res[i+9]:
                north   = res[i+3].split(",")
                east    = res[i+4].split(",")
                south   = res[i+5].split(",")
                west    = res[i+6].split(",")

                north_avg = float(north[1].split()[2])
                north_max = float(north[2].split()[2])

                east_avg = float(east[1].split()[2])
                east_max = float(east[2].split()[2])

                south_avg = float(south[1].split()[2])
                south_max = float(south[2].split()[2])

                west_avg = float(west[1].split()[2])
                west_max = float(west[2].split()[2])

                simulation_time = float(res[i+8].split()[1])

                overall_avg = (north_avg + east_avg + south_avg + west_avg)/4

                if simulation_time < 4:
                    grade+=1.5

                if (    (north_max < L3_EFFICIENCY and east_max < L3_EFFICIENCY and south_max < L3_EFFICIENCY and west_max < L3_EFFICIENCY) and
                        (abs(north_avg - overall_avg) < L3_GAP and abs(east_avg - overall_avg) < L3_GAP and
                        abs(south_avg - overall_avg) < L3_GAP and abs(west_avg - overall_avg) < L3_GAP)
                    ):
                    grade += 1.5

                if grade < 1.5:
                    cmts.add("The output of heavy-load-3-no-bias is wrong:")
            else:
                cmts.add("The output of heavy-load-3-no-bias is too long or incorrect.")

        except Exception as e:
            cmts.add("Cannot parse assignment log - likely an error in your assignment")
        
    if grade < 9:
       cmts.add("The output of heavy-load-3-no-bias sp3 10 80 0 1 0 does not meet efficiency thresholds set")

    return ("Test heavy-load-3-no-bias", grade, ",".join(cmts))

def heavy_load3_with_bias(tests, helpers):
    grade = 0
    cmts = set()
    mytests = [t for t in tests if "heavy-load-3-with-bias" in t.name]
    if len(mytests) != 3:
        cmts.add("Not all configurations were run - likly timed out")

    for test in mytests:
        try:
            res = test.results().split('\n')
            i = res.index("OS/161 kernel: sp3 10 80 0 1 1")

            if "Operation took" in res[i+1]:
                cmts.add("The output of heavy-load-3-with-bias is empty.")
            elif "Operation took" in res[i+9]:
                north   = res[i+3].split(",")
                east    = res[i+4].split(",")
                south   = res[i+5].split(",")
                west    = res[i+6].split(",")

                north_avg = float(north[1].split()[2])
                north_max = float(north[2].split()[2])

                east_avg = float(east[1].split()[2])
                east_max = float(east[2].split()[2])

                south_avg = float(south[1].split()[2])
                south_max = float(south[2].split()[2])

                west_avg = float(west[1].split()[2])
                west_max = float(west[2].split()[2])

                simulation_time = float(res[i+8].split()[1])

                if simulation_time < 4:
                    grade+=1

                if (north_max < L3_EFFICIENCY and
                    east_max < L3_EFFICIENCY and
                    south_max < L3_EFFICIENCY and
                    west_max < L3_EFFICIENCY):
                    grade += 1

                if grade < 1:
                    cmts.add("The output of heavy-load-3-with-bias is wrong:")
            else:
                cmts.add("The output of heavy-load-3-with-bias is too long or incorrect.")

        except Exception as e:
            cmts.add("Cannot parse assignment log - likely an error in your assignment")
        
    if grade < 6:
       cmts.add("The output of heavy-load-3-with-bias sp3 10 80 0 1 1 does not meet efficiency thresholds set")

    return ("Test heavy-load-3-with-bias", grade, ",".join(cmts))

RUBRIC = {
    sy2: 24,
    sy3: 24,
    uw1: 24,
    single_vehicle_no_bias: 6,
    single_vehicle_with_bias: 6,
    light_load_no_bias: 9,
    light_load_with_bias: 6,
    heavy_load1_no_bias: 9,
    heavy_load1_with_bias: 6,
    heavy_load2_no_bias: 9,
    heavy_load2_with_bias: 6,
    heavy_load3_no_bias: 9,
    heavy_load3_with_bias: 6
}
