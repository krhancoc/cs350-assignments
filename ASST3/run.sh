#!/bin/bash
source /helpers.sh

LOG=$LOGS/test_public.log
COMPILE=$LOGS/compile.log
ASSIGNMENTDIR=/assignments/ASST3

touch $COMPILE
truncate -s 0 $COMPILE

if [ ! -d /kernel ];
then
	unpack_kernel >> $COMPILE 2>> $COMPILE
fi

echo "[CS350] Building Kernel"

build_kernel ASST3 >> $COMPILE 2>> $COMPILE

touch $LOG
truncate -s 0 $LOG

cd /os-compile
echo "[CS350] Testing ASST3"

c="$ASSIGNMENTDIR/basic_vm_integrity.conf"
for f in $ASSIGNMENTDIR/basic/basic_vm_integrity/*
do
    echo ">SPLIT<" >> $LOG
    cname=$(basename -s .conf $c)
    fname=$(basename $f)
    echo ">TEST=${fname}_${cname}<"  >> $LOG
    echo ">Program Output<" >> $LOG
    P=$(cat $f)
    timeout --foreground 60s sys161 -c $c kernel "$P" >> $LOG 2>> $LOG
    if [ $? -eq 124 ]; 
    then
	echo "$P has timed out after 60s, this should not occur. Most likely a deadlock" >> $LOG
    fi
done

c="$ASSIGNMENTDIR/romem.conf"
for f in $ASSIGNMENTDIR/basic/read_only_memory_tests/*
do
      echo ">SPLIT<" >> $LOG
      cname=$(basename -s .conf $c)
      fname=$(basename $f)
      echo ">test=${fname}_${cname}<"  >> $LOG
      echo ">program output<" >> $LOG
      P=$(cat $f)
      timeout --foreground 60s sys161 -c $c kernel "$P" >> $LOG 2>> $LOG
      if [ $? -eq 124 ]; 
      then
          echo "$P has timed out after 60s, this should not occur. most likely a deadlock" >> $LOG
      fi
done


c="$ASSIGNMENTDIR/phy_mem.conf"
for f in $ASSIGNMENTDIR/basic/physical_mem_tests/*
do
      echo ">SPLIT<" >> $LOG
      cname=$(basename -s .conf $c)
      fname=$(basename $f)
      echo ">test=${fname}_${cname}<"  >> $LOG
      echo ">program output<" >> $LOG
      P=$(cat $f)
      timeout --foreground 60s sys161 -c $c kernel "$P" >> $LOG 2>> $LOG
      if [ $? -eq 124 ]; 
      then
          echo "$P has timed out after 60s, this should not occur. most likely a deadlock" >> $LOG
      fi
done

c="$ASSIGNMENTDIR/mp.conf"
for f in $ASSIGNMENTDIR/basic/multiprocess_tests/*
do
      echo ">SPLIT<" >> $LOG
      cname=$(basename -s .conf $c)
      fname=$(basename $f)
      echo ">test=${fname}_${cname}<"  >> $LOG
      echo ">program output<" >> $LOG
      P=$(cat $f)
      timeout --foreground 60s sys161 -c $c kernel "$P" >> $LOG 2>> $LOG
      if [ $? -eq 124 ];
      then
          echo "$P has timed out after 60s, this should not occur. most likely a deadlock" >> $LOG
      fi
done

exit 0
